Adopy
=====
A simple module for reading and writing ado files. IDF is a simple flat format used by the Triwaco groundwater modelling software.

Installation requires Python 3+ and Numpy. Pytest is required for testing.

Usage
-----

Example:
::

