import functools
import time


def timed(msg='{fn} took {duration:.3f}s'):
    def decorator(fn):
        @functools.wraps(fn)
        def inner(*args, **kwargs):
            start = time.time()
            result = fn(*args, **kwargs)
            duration = time.time() - start
            print(msg.format(fn=fn.__name__, duration=duration))
            return result
        return inner
    return decorator
