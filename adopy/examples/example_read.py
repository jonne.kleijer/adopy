#! /usr/bin/env python
# -*- coding: utf-8 -*-

import adopy
from adopy.helpers import timed


@timed()
def example_read_times():
    samplefile = r'DA1.ado'
    with adopy.open(samplefile) as src:
        print(src)
        print(src.times)


@timed()
def example_read_single_time():
    samplefile = r'DA1.ado'
    with adopy.open(samplefile) as src:
        print(src.read(time=0))


@timed()
def example_read_two_times():
    samplefile = r'DA1.ado'
    with adopy.open(samplefile) as src:
        print(src.read(time=0))
        print(src.read(time=len(src.times) - 1))


@timed()
def example_read_single_value_timestep():
    samplefile = r'DA1_diff_blocks.ado'
    with adopy.open(samplefile) as src:
        print(src.read(time=0))
        print(src.read(time=len(src.times) - 1))


def main():
    # example_read_times()
    # example_read_single_time()
    # example_read_two_times()
    example_read_single_value_timestep()


if __name__ == '__main__':
    main()
