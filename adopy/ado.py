#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import struct

import numpy as np

MSG = '{f} is not yet implemented in {self}'


class AdoFile(object):
    """Ado file read object"""

    def __init__(self, filepath, mode='r', start=None, grid=None):
        self.filepath = filepath
        self.charwidth = 72
        self.open(mode=mode)
        self.start = start
        self.grid = None

        self._content = None
        self._times = None
        self._times_line = None

    def __enter__(self):
        """enter with statement block"""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """exit with statement block"""
        self.close()

    def __repr__(self):
        return (
            "{self.__class__.__name__:}(filepath={self.filepath:}, "
            "mode='{self.mode:}')"
        ).format(self=self, )

    @property
    def closed(self):
        return self.f.closed

    @property
    def mode(self):
        return self.f.mode

    @property
    def content(self):
        if self._content is None:
            self._content = self.f.read().splitlines()
        return self._content

    @property
    def _width(self):
        return len(self.content[0])

    @property
    def _numbers(self):
        for line in self._content:
            if re.match('( \d.\d+E\+\d{2})+', line):
                return len(re.findall(' \d.\d+E\+\d{2}', line))

    @property
    def _widthnumber(self):
        return self._width // self._numbers

    @property
    def times(self):
        # TODO: Parse to datetime when provided with start
        # TODO: Seperate self._times_line from self._times
        if self._times is None:
            self._times = []
            self._times_line = []
            for i, line in enumerate(self.content):
                if line.startswith('*SET*'):
                    self._times.append(float(re.findall('\d+\.\d+', line)[0]))
                    self._times_line.append(i)
        return np.array(self._times)

    # @property
    # def _adostruct(self):
    #     fieldwidths = (2, -10, 24)
    #     fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw < 0 else 's')
    #                          for fw in fieldwidths)
    #     fieldstruct = struct.Struct(fmtstring)

    @property
    def timesteps(self):
        return len(self.times)

    def open(self, mode='r'):
        """open file handle"""
        self.f = open(self.filepath, mode)

    def close(self):
        """close file handle"""
        self.f.close()

    def check_read(self):
        """check if reading from file is ok"""
        if self.mode != 'r':
            raise ValueError("cannot read in '{}' mode".format(self.mode))
        if self.closed:
            raise IOError('cannot read closed ado file')
        return True

    def read(self, time=None):
        # TODO assert (85600 == len(values)
        # TODO use struct get pack as binary --> test speed
        # TODO different solution to assert self.times to make sure content and times properties exist
        # TODO more elegant solution to read data blocks (similar to times?)
        # TODO dynamic + 3 and - 2 for start- and endline

        assert self.times is not None
        if time is None:
            time = self.times[0]
        startline = self._times_line[time] + 3
        if time == len(self.times) - 1:
            endline = len(self._content) - 1
        else:
            endline = self._times_line[time + 1] - 2
        data = self.content[startline:endline]
        values = []
        for line in data:
            values.extend((line[0 + i:self._widthnumber + i] for i in range(0, self._width, self._widthnumber)))
        return np.array(values)


class GridFile(AdoFile):
    def __init__(self):
        NotImplementedError
