# -*- coding: utf-8 -*-

from .ado import AdoFile


def open(adofile, mode='r'):
    """AdoFile instance from file"""
    return AdoFile(adofile, mode=mode)
